<?php
/*
Plugin Name: Insertar Google Tag Manager en WordPress
Plugin URI: https://e451.net
Description: Plugin para insertar los Scripts de Google Tag Manager
Version: 1.0.0
Author: Aitor Méndez
License: GPL 3
*/
// Añadir el código de Google Tag Manager en el <head>
add_action( 'wp_head', 'e451_google_tag_manager1' );
function e451_google_tag_manager1() { ?>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5RK5FHJ');</script>
    <!-- End Google Tag Manager -->

<?php }
// Añadir el código de Google Tag Manager code justo  debajo de la apertura de la etiqueta <body>
add_action( 'wp_footer', 'e451_google_tag_manager2' );
function e451_google_tag_manager2() { ?>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5RK5FHJ"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

<?php }

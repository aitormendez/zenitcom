<?php
   /*
   Plugin Name: zenitcom
   Text Domain: zenitcom
   Plugin URI:
   Description:
   Version: 1.0
   Author: Aitor
   Author URI: https://e451.net
   License: GPL 3.0
   */
?>
<?php

// eliminar taxonomías por defecto
function zc_unregister_taxonomy(){
    register_taxonomy('category', array());
}
add_action('init', 'zc_unregister_taxonomy');

// eliminar post formats
add_action( 'init', 'zc_remove_post_type_support', 10 );
function zc_remove_post_type_support() {
    remove_post_type_support( 'post', 'post-formats' );
}

// eliminar post type "posts" (entradas por defecto)
add_action('admin_menu','remove_default_post_type');
function remove_default_post_type() {
	remove_menu_page('edit.php');
}

// Register Custom Taxonomy
function servicios_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Servicios', 'Taxonomy General Name', 'zenitcom' ),
		'singular_name'              => _x( 'Servicio', 'Taxonomy Singular Name', 'zenitcom' ),
		'menu_name'                  => __( 'Servicios', 'zenitcom' ),
		'all_items'                  => __( 'Todos los servicios', 'zenitcom' ),
		'parent_item'                => __( 'Servicio padre', 'zenitcom' ),
		'parent_item_colon'          => __( 'Servicio padre:', 'zenitcom' ),
		'new_item_name'              => __( 'Nuevo nombre de servicio', 'zenitcom' ),
		'add_new_item'               => __( 'Añadir nuevo servicio', 'zenitcom' ),
		'edit_item'                  => __( 'Editar servicio', 'zenitcom' ),
		'update_item'                => __( 'Actualizar servicio', 'zenitcom' ),
		'view_item'                  => __( 'Ver servicio', 'zenitcom' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'zenitcom' ),
		'add_or_remove_items'        => __( 'Añadir o eliminar servicios', 'zenitcom' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'zenitcom' ),
		'popular_items'              => __( 'Popular Items', 'zenitcom' ),
		'search_items'               => __( 'Buscar servicios', 'zenitcom' ),
		'not_found'                  => __( 'Not Found', 'zenitcom' ),
		'no_terms'                   => __( 'No items', 'zenitcom' ),
		'items_list'                 => __( 'Items list', 'zenitcom' ),
		'items_list_navigation'      => __( 'Items list navigation', 'zenitcom' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'servicios', array( 'trabajo' ), $args );

}
add_action( 'init', 'servicios_taxonomy', 0 );

// Register Custom Taxonomy
function clientes_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Clientes', 'Taxonomy General Name', 'zenitcom' ),
		'singular_name'              => _x( 'Cliente', 'Taxonomy Singular Name', 'zenitcom' ),
		'menu_name'                  => __( 'Clientes', 'zenitcom' ),
		'all_items'                  => __( 'Todos los clientes', 'zenitcom' ),
		'parent_item'                => __( 'Cliente padre', 'zenitcom' ),
		'parent_item_colon'          => __( 'Servicio padre:', 'zenitcom' ),
		'new_item_name'              => __( 'Nuevo nombre de cliente', 'zenitcom' ),
		'add_new_item'               => __( 'Añadir nuevo cliente', 'zenitcom' ),
		'edit_item'                  => __( 'Editar cliente', 'zenitcom' ),
		'update_item'                => __( 'Actualizar cliente', 'zenitcom' ),
		'view_item'                  => __( 'Ver clientes', 'zenitcom' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'zenitcom' ),
		'add_or_remove_items'        => __( 'Añadir o eliminar clientes', 'zenitcom' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'zenitcom' ),
		'popular_items'              => __( 'Popular Items', 'zenitcom' ),
		'search_items'               => __( 'Buscar clientes', 'zenitcom' ),
		'not_found'                  => __( 'Not Found', 'zenitcom' ),
		'no_terms'                   => __( 'No items', 'zenitcom' ),
		'items_list'                 => __( 'Items list', 'zenitcom' ),
		'items_list_navigation'      => __( 'Items list navigation', 'zenitcom' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'clientes', array( 'trabajo' ), $args );

}
add_action( 'init', 'clientes_taxonomy', 0 );


// Register Custom Post Type
function trabajo_post_type() {

	$labels = array(
		'name'                  => _x( 'Trabajos', 'Post Type General Name', 'zenitcom' ),
		'singular_name'         => _x( 'Trabajo', 'Post Type Singular Name', 'zenitcom' ),
		'menu_name'             => __( 'Trabajos', 'zenitcom' ),
		'name_admin_bar'        => __( 'Trabajo', 'zenitcom' ),
		'archives'              => __( 'Archivo de trabajos', 'zenitcom' ),
		'attributes'            => __( 'Atributo de trabajo', 'zenitcom' ),
		'parent_item_colon'     => __( 'Trabajo padre:', 'zenitcom' ),
		'all_items'             => __( 'Todos los trabajos', 'zenitcom' ),
		'add_new_item'          => __( 'Añadir nuevo trabajo', 'zenitcom' ),
		'add_new'               => __( 'Añadir nuevo', 'zenitcom' ),
		'new_item'              => __( 'Nuevo trabajo', 'zenitcom' ),
		'edit_item'             => __( 'Editar trabajo', 'zenitcom' ),
		'update_item'           => __( 'Actualizar trabajo', 'zenitcom' ),
		'view_item'             => __( 'Ver trabajo', 'zenitcom' ),
		'view_items'            => __( 'Ver trabajos', 'zenitcom' ),
		'search_items'          => __( 'Buscar trabajo', 'zenitcom' ),
		'not_found'             => __( 'Not found', 'zenitcom' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'zenitcom' ),
		'featured_image'        => __( 'Featured Image', 'zenitcom' ),
		'set_featured_image'    => __( 'Set featured image', 'zenitcom' ),
		'remove_featured_image' => __( 'Remove featured image', 'zenitcom' ),
		'use_featured_image'    => __( 'Use as featured image', 'zenitcom' ),
		'insert_into_item'      => __( 'Insert into item', 'zenitcom' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'zenitcom' ),
		'items_list'            => __( 'Lista de trabajos', 'zenitcom' ),
		'items_list_navigation' => __( 'Items list navigation', 'zenitcom' ),
		'filter_items_list'     => __( 'Filter items list', 'zenitcom' ),
	);
	$args = array(
		'label'                 => __( 'Trabajo', 'zenitcom' ),
		'description'           => __( 'Post Type Description', 'zenitcom' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail' ),
		'taxonomies'            => array( 'servicios', 'clientes', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
    'menu_icon'             => 'dashicons-hammer',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'trabajo', $args );

}
add_action( 'init', 'trabajo_post_type', 0 );

// Register Custom Post Type
function noticia_post_type() {

	$labels = array(
		'name'                  => _x( 'Noticias', 'Post Type General Name', 'zenitcom' ),
		'singular_name'         => _x( 'Noticia', 'Post Type Singular Name', 'zenitcom' ),
		'menu_name'             => __( 'Noticias', 'zenitcom' ),
		'name_admin_bar'        => __( 'Noticia', 'zenitcom' ),
		'archives'              => __( 'Archivo de noticias', 'zenitcom' ),
		'attributes'            => __( 'Atributo de noticia', 'zenitcom' ),
		'parent_item_colon'     => __( 'Noticia padre:', 'zenitcom' ),
		'all_items'             => __( 'Todas las noticias', 'zenitcom' ),
		'add_new_item'          => __( 'Añadir nueva noticia', 'zenitcom' ),
		'add_new'               => __( 'Añadir nueva', 'zenitcom' ),
		'new_item'              => __( 'Nueva noticia', 'zenitcom' ),
		'edit_item'             => __( 'Editar noticia', 'zenitcom' ),
		'update_item'           => __( 'Actualizar noticia', 'zenitcom' ),
		'view_item'             => __( 'Ver noticia', 'zenitcom' ),
		'view_items'            => __( 'Ver noticias', 'zenitcom' ),
		'search_items'          => __( 'Buscar noticia', 'zenitcom' ),
		'not_found'             => __( 'Not found', 'zenitcom' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'zenitcom' ),
		'featured_image'        => __( 'Featured Image', 'zenitcom' ),
		'set_featured_image'    => __( 'Set featured image', 'zenitcom' ),
		'remove_featured_image' => __( 'Remove featured image', 'zenitcom' ),
		'use_featured_image'    => __( 'Use as featured image', 'zenitcom' ),
		'insert_into_item'      => __( 'Insert into item', 'zenitcom' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'zenitcom' ),
		'items_list'            => __( 'Lista de noticias', 'zenitcom' ),
		'items_list_navigation' => __( 'Items list navigation', 'zenitcom' ),
		'filter_items_list'     => __( 'Filter items list', 'zenitcom' ),
	);
	$args = array(
		'label'                 => __( 'Noticia', 'zenitcom' ),
		'description'           => __( 'Noticias', 'zenitcom' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail' ),
		'taxonomies'            => array( 'clientes', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
    'menu_icon'             => 'dashicons-megaphone',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'noticia', $args );

}
add_action( 'init', 'noticia_post_type', 0 );


// añadir image size a admin_menu
// add_filter( 'image_size_names_choose', 'my_custom_sizes' );
//
// function my_custom_sizes( $sizes ) {
//     return array_merge( $sizes, array(
//         'hero-600' => __( 'hero-600' ),
//     ) );
// }


// eliminar limitacion máxima de imagen
// add_filter( 'max_srcset_image_width', 'max_srcset_image_width', 10 , 2 );
// function max_srcset_image_width() {
//     return 2048;
// }


// eliminar "Clientes" y "Servicios" del título
function filter_title($title) {
  if (is_tax('servicios')) {
    return str_replace('Servicio: ', '', $title);
  } elseif (is_tax('clientes')) {
    return str_replace('Cliente: ', '', $title);
  }
}
add_filter('get_the_archive_title', __NAMESPACE__ . '\\filter_title');

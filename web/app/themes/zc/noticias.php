<?php
/**
 * Template Name: Noticias
 */
?>
<?php get_template_part('templates/page', 'header'); ?>

<div class="columna">

<?php
$args = array(
	'post_type'       => array( 'noticia' ),
	'post_status'     => array( 'publish' ),
	'posts_per_page'  => '12',
  'paged' => ( get_query_var('paged') ) ? get_query_var('paged') : 1,

);

$noticias_query = new WP_Query( $args );

if ( $noticias_query->have_posts() ) {
	while ( $noticias_query->have_posts() ) {
		$noticias_query->the_post(); ?>
		  <?php get_template_part('templates/content', 'noticias'); ?>
	<?php }
}
wp_reset_postdata(); ?>

</div>
<div class="columna">
	<ul class="nav" role="navigation">
	   <li><?php echo get_next_posts_link( 'Noticias anteriores', $noticias_query->max_num_pages ); ?></li>
	   <li><?php echo get_previous_posts_link( 'Noticias posteriores' ); ?></li>
	</ul>
</div>

/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {

      init: function() {
        // JavaScript to be fired on all pages


        $( document ).ready(function(){
          // boton hamburguesa
          $(".hamb").click(function(){
            $(".menu").toggleClass("active");
            $(".spanf ").toggleClass("span1 ");
            $(".spans ").toggleClass("span2 ");
            $(".spant ").toggleClass("span3 ");
          });

          // boton desplegar búsquedas
          $(".buscar a, .busquedas .s-cerrar-1, .busquedas .btn-icono").click(function(){
            $(".busquedas").toggleClass("active");
          });




        });



      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page

        $( document ).ready(function(){
          var viewportWidth = $(window).width();
          if (viewportWidth >= 1000) {


            // añadir y quitar clase "peq" en banner cuando scroll
            $(window).scroll(function() {
              var scroll = $(window).scrollTop();
            	if (scroll >= 1) {
                $(".banner").addClass("peq", 1000);
              } else {
                $(".banner").removeClass("peq", 1000);
              }
            });
          }


          $('.owl-carousel').owlCarousel({
            loop:true,
            autoplay:true,
            autoplayTimeout: 10000,
            autoheight: true,
            responsive:{
                0:{
                    items:1
                },
                1200:{
                    items:2
                },
                1800:{
                    items:3
                }
            }
          });


        });


      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'single': {
      init: function() {


        $(document).ready(function(){
        $('.gallery').justifiedGallery({
          rowHeight : 100,
          margins : 3,
          cssAnimation: true
        }).on('jg.complete', function(){
          $(this).lightGallery({
            thumbnail: true
          });
        });
      });


      }
    },
    // About us page, note the change from about-us to about_us.
    'contacto': {
      init: function() {
        $(document).ready(function(){

          var colorcruz = '#535353';





          // $(function initMap() {
          //   var zenitcom = {lat: 40.4209269, lng: -3.7040943};
          //   var mapDiv = document.getElementById('map');
          //   var map = new google.maps.Map(mapDiv, {
          //     center: zenitcom,
          //     zoom: 17,
          //     styles: [
          //       {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
          //       {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
          //       {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
          //       {
          //         featureType: 'administrative.locality',
          //         elementType: 'labels.text.fill',
          //         stylers: [{color: '#d59563'}]
          //       },
          //       {
          //         featureType: 'poi',
          //         elementType: 'labels.text.fill',
          //         stylers: [{color: '#d59563'}]
          //       },
          //       {
          //         featureType: 'poi.park',
          //         elementType: 'geometry',
          //         stylers: [{color: '#263c3f'}]
          //       },
          //       {
          //         featureType: 'poi.park',
          //         elementType: 'labels.text.fill',
          //         stylers: [{color: '#6b9a76'}]
          //       },
          //       {
          //         featureType: 'road',
          //         elementType: 'geometry',
          //         stylers: [{color: '#38414e'}]
          //       },
          //       {
          //         featureType: 'road',
          //         elementType: 'geometry.stroke',
          //         stylers: [{color: '#212a37'}]
          //       },
          //       {
          //         featureType: 'road',
          //         elementType: 'labels.text.fill',
          //         stylers: [{color: '#9ca5b3'}]
          //       },
          //       {
          //         featureType: 'road.highway',
          //         elementType: 'geometry',
          //         stylers: [{color: '#746855'}]
          //       },
          //       {
          //         featureType: 'road.highway',
          //         elementType: 'geometry.stroke',
          //         stylers: [{color: '#1f2835'}]
          //       },
          //       {
          //         featureType: 'road.highway',
          //         elementType: 'labels.text.fill',
          //         stylers: [{color: '#f3d19c'}]
          //       },
          //       {
          //         featureType: 'transit',
          //         elementType: 'geometry',
          //         stylers: [{color: '#2f3948'}]
          //       },
          //       {
          //         featureType: 'transit.station',
          //         elementType: 'labels.text.fill',
          //         stylers: [{color: '#d59563'}]
          //       },
          //       {
          //         featureType: 'water',
          //         elementType: 'geometry',
          //         stylers: [{color: '#17263c'}]
          //       },
          //       {
          //         featureType: 'water',
          //         elementType: 'labels.text.fill',
          //         stylers: [{color: '#515c6d'}]
          //       },
          //       {
          //         featureType: 'water',
          //         elementType: 'labels.text.stroke',
          //         stylers: [{color: '#17263c'}]
          //       }
          //     ]

          //   });
          //   var marker = new google.maps.Marker({
          //     position: {lat: 40.4209269, lng: -3.7040943},
          //     map: map,
          //     icon: {
          //       path: 'M14.8492424,12.0208153 L23.3345238,3.53553391 C23.7250481,3.14500961 23.7250481,2.51184464 23.3345238,2.12132034 L21.9203102,0.707106781 C21.5297859,0.316582489 20.8966209,0.316582489 20.5060967,0.707106781 L12.0208153,9.19238816 L3.53553391,0.707106781 C3.14500961,0.316582489 2.51184464,0.316582489 2.12132034,0.707106781 L0.707106781,2.12132034 C0.316582489,2.51184464 0.316582489,3.14500961 0.707106781,3.53553391 L9.19238816,12.0208153 L0.707106781,20.5060967 C0.316582489,20.8966209 0.316582489,21.5297859 0.707106781,21.9203102 L2.12132034,23.3345238 C2.51184464,23.7250481 3.14500961,23.7250481 3.53553391,23.3345238 L12.0208153,14.8492424 L19.7989899,22.627417 C20.1895142,23.0179413 20.8226791,23.0179413 21.2132034,22.627417 L22.627417,21.2132034 C23.0179413,20.8226791 23.0179413,20.1895142 22.627417,19.7989899 L14.8492424,12.0208153 Z',
          //       scale: 1,
          //       anchor: new google.maps.Point(16, 16),
          //       fillColor: '#d2ff1d',
          //       fillOpacity: 1,
          //       strokeWeight: 0
          //     }
          //   });
          //   var contentString =
          //     '<div class="infowindow">'+'<h1 id="firstHeading" class="firstHeading">Zenit Comunicación</h1>'+'<p>Desengaño, 11. 2º ext - 2B</p>'+'<p>28004 Madrid'+'<p>(+34) 91 559 91 88</p>'+'</div>';
          //   var infowindow = new google.maps.InfoWindow({
          //     content: contentString
          //   });
          //   marker.addListener('click', function() {
          //     infowindow.open(map, marker);
          //   });
          // });
        });
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.


<div class="trabajos-destacados">
  <div class="owl-carousel owl-theme">
    <?php
    $args = array(
      'post_type'              => array( 'trabajo' ),
      'post_status'            => array( 'publish' ),
      'posts_per_page'         => -1,
      'meta_query' => array(
        array(
          'key' => 'portada', // name of custom field
          'value' => 'portada', // matches exactly "portada"
          'compare' => 'LIKE'
        )
      )
    );
    $posts = get_posts($args);

    if( $posts ) {
      foreach ( $posts as $post ) : setup_postdata( $post );
      ?>
        <a role="article" href="<?php the_permalink(); ?>">
          <div class="cont-imagen">
            <div class="mask" aria-hidden="true"></div>
            <?php if (has_excerpt()) { ?>
              <div class="resumen">
                <p>
                  <?php echo wp_trim_words( get_the_excerpt(), 50, '...' ); ?>
                </p>
              </div>
            <?php } ?>
            <?php the_post_thumbnail('i1000-500'); ?>
          </div>
          <h1><?php the_title(); ?></h1>
        </a>
        <?php endforeach;
        wp_reset_postdata();
    } ?>
  </div>
</div>


<!--
Declaro la función para coger el título del trabajo a que pertenece cada noticia
Todo esto es para mostrar el trabajo asociado en la noticia

El problema está en que hay un wp_query dentro de otro. Al final, tengo que
guardar título en una variable porque la segunda query me machaca la variable
$post de la primera (linea 92)
-->
<?php
function get_related_posts($id){
  // Get current post's tags
  $tags = wp_get_post_tags($id);
  // Check if the post has any tags
  if ($tags) {
      // If it does, get them and make a query based on tags
      $tag_ids = array();
      foreach($tags as $individual_tag) {
          $tag_ids[] = $individual_tag->term_id;
      }
      $args_tags = array(
          'tag__in' => $tag_ids,
          'post__not_in' => array($id),
          'posts_per_page'=>-1,
          'post_type' => 'trabajo',
      );
      $related_query = new WP_Query( $args_tags );
      // Starting the loop
      if ( $related_query->have_posts()) {
         while ($related_query->have_posts()){
            $related_query->the_post(); ?>
            <div class="mask" aria-hidden="true"></div>
            <div class="trabajo">
              <p><?php the_title(); ?></p>
            </div>

          <?php } ?>
        <?php }
      }
      wp_reset_postdata();
    }; ?>

<div class="noticias-destacadas">
  <?php
  $args_noticias = array(
    'post_type'              => array( 'noticia' ),
    'post_status'            => array( 'publish' ),
    'posts_per_page'         => 10,
  );
  $noticias_query = new WP_Query( $args_noticias );
  // Starting the loop
  if ( $noticias_query->have_posts()) {
     while ($noticias_query->have_posts()){
        $noticias_query->the_post();
        $titulo = get_the_title()?>
        <a role="article" href="<?php the_permalink(); ?>">
          <div class="cont-imagen">
            <?php the_post_thumbnail('i600-300'); ?>
            <?php get_related_posts(get_the_ID()); ?>
          </div>
          <h1><?= $titulo ?></h1>

        </a>
      <?php } ?>
    <?php }
   ?>
</div>

<header class="banner">

  <nav class="nav-sec">
    <ul>
      <li class="social">
        <a href="https://www.facebook.com/zenit.comunicacion" target="_blank" rel="external">
          <span class="s s-facebook-i"></span>
        </a>
        <a href="https://twitter.com/zenitcom" target="_blank" rel="external">
          <span class="s s-twitter-i"></span>
        </a>
        <a href="https://www.instagram.com/zenitcomunicacion/" target="_blank" rel="external">
          <span class="s s-instagram-i"></span>
        </a>
      </li>
      <li class="buscar">
        <a type="button">
          <span class="s s-buscar-1"></span>
        </a>
      </li>
      <li class="contacto">
        <a href="<?php echo get_site_url().'/contacto'; ?>">
          <span class="s s-contacto-1"></span>
        </a>
      </li>
      <li class="ham">
        <div class="hamb">
          <span class="spanf">
            <i></i>
          </span><span class="spans">
            <i></i>
          </span><span class="spant">
            <i></i>
          </span>
        </div>
      </li>
    </ul>

  </nav>
  <a class="brand" href="<?= esc_url(home_url('/')); ?>">
    <div class="logo">
      <div class="logosimbolo">
        <img src="<?= esc_url(home_url('app/themes/zc/dist/images/logo-simbolo.svg')); ?>" alt="Zenit Comunicación" class="logo-simbolo">
        <img src="<?= esc_url(home_url('app/themes/zc/dist/images/logo-tipo.svg')); ?>" alt="Zenit Comunicación" class="logo-tipo">
      </div>

      <p>COMUNICACIÓN</p>
    </div>

  </a>
  <nav class="nav-prin">
    <?php
    if (has_nav_menu('primary_navigation')) :
      wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'menu']);
    endif;
    ?>
  </nav>
  <div class="busquedas">
    <a class="s s-cerrar-1"></a>
    <?php get_search_form(); ?>
  </div>
</header>

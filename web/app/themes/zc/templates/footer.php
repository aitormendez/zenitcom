<footer class="content-info">
  <div class="container">
    <?php dynamic_sidebar('sidebar-footer'); ?>
    <section class="social">
      <h3>Social</h3>
      <ul>
        <li>
          <a href="https://www.facebook.com/zenit.comunicacion" target="_blank">
            <span class="s s-facebook-i"></span>
          </a>
        </li>
        <li>
          <a href="https://twitter.com/zenitcom" target="_blank">
            <span class="s s-twitter-i"></span>
          </a>
        </li>
        <li>
          <a href="https://www.instagram.com/zenitcomunicacion/" target="_blank">
            <span class="s s-instagram-i"></span>
          </a>
        </li>
      </ul>
    </section>
    <section class="zenit">
      <h3>Zenit comunicación</h3>
      <p>Cuesta de San Vicente, 30. 7º C.</p>
      <p>28008 Madrid</p>
    </section>
  </div>
  <div class="container-mini">
    <p><a href="https://e451.net" target="_blank">Diseño y desarrollo web: 451</a></p>
  </div>
</footer>

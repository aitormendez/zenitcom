<?php

if (is_page('trabajos')) {
  echo get_the_term_list( $post->ID, 'clientes', '<ul class="cliente"><li>', ',</li><li>', '</li></ul>' );
  echo get_the_term_list( $post->ID, 'servicios', '<ul class="servicios"><li>', '</li><li>', '</li></ul>' );
} elseif (is_tax('clientes')) {
  echo get_the_term_list( $post->ID, 'servicios', '<ul class="servicios"><li>', '</li><li>', '</li></ul>' );
} elseif (is_tax('servicios')) {
  echo get_the_term_list( $post->ID, 'clientes', '<ul class="cliente"><li>', ',</li><li>', '</li></ul>' );
} elseif (is_page('noticias')) {
  echo get_the_term_list( $post->ID, 'clientes', '<ul class="cliente"><li>', ',</li><li>', '</li></ul>' );
} elseif (is_search()) {
  echo get_the_term_list( $post->ID, 'clientes', '<ul class="cliente"><li>', ',</li><li>', '</li></ul>' );
  echo get_the_term_list( $post->ID, 'servicios', '<ul class="servicios"><li>', '</li><li>', '</li></ul>' );
} else {
  echo get_the_term_list( $post->ID, 'clientes', '<ul class="cliente"><li>', ',</li><li>', '</li></ul>' );
  echo get_the_term_list( $post->ID, 'servicios', '<ul class="servicios"><li>', ' | </li><li>', '</li></ul>' );
}
?>

<?php
if (is_page('noticias') || is_singular('noticia')) { ?>
  <time><?php the_time('F Y'); ?></time>
<?php } else {
  $unixtimestamp = strtotime(get_field('fecha'));
  $fecha = date_i18n("F Y", $unixtimestamp);
  $fecha_iso = date_i18n("c", $unixtimestamp);
  ?>

  <time class="updated" datetime="<?= $fecha_iso; ?>">
    <?= $fecha; ?>
  </time>
<?php } ?>

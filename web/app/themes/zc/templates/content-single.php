<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>

    <header>
      <div class="metadatos columna">
        <h1 class="entry-title"><?php the_title(); ?></h1>
        <?php get_template_part('templates/entry-meta'); ?>
        <aside class="navegacion">
          <ul>
              <?php
              function get_related_posts_menu($id){
                // Get current post's tags
                $tags = wp_get_post_tags($id);
                // Check if the post has any tags
                if ($tags) {
                    // If it does, get them and make a query based on tags
                    $tag_ids = array();
                    foreach($tags as $individual_tag) {
                        $tag_ids[] = $individual_tag->term_id;
                    }
                    $args = array(
                        'tag__in'             => $tag_ids,
                        'post__not_in'        => array($id),
                        'posts_per_page'      => -1,
                        'ignore_sticky_posts' => 1,
                        'post_type'           => 'noticia',
                        'post_status'         => array( 'publish' )
                    );
                    $related_query = new WP_Query( $args );
                    // Starting the loop
                    if ( $related_query->have_posts()) { ?>
                      <li>
                        <a href="#noticias">Noticias</a>
                      </li>
                      <?php }
                    }
                    wp_reset_postdata();
                  }
                  get_related_posts_menu(get_the_ID());?>
            <?php
            $tags = wp_get_post_tags($id);
            if (get_field('galerias')) { ?>
              <li>
                <a href="#media">Media</a>
              </li>
            <?php } ?>
          </ul>
        </aside>
      </div>

    </header>


      <?php if ( has_post_thumbnail() ) { ?>
        <div class="imagen">
        <?php the_post_thumbnail('i1200', ['class' => 'columna']); ?>
        </div>
      <?php } ?>


    <?php if (has_excerpt()) { ?>
      <div class="excerpt">
        <div class="columna">
          <?php the_excerpt(); ?>
        </div>
      </div>
    <?php } ?>

    <?php if (get_the_content()) { ?>
      <div class="entry-content">
        <div class="columna">
          <?php the_content(); ?>
        </div>
      </div>
    <?php } ?>




      <?php
      function get_related_posts($id){
        // Get current post's tags
        $tags = wp_get_post_tags($id);
        // Check if the post has any tags
        if ($tags) {
            // If it does, get them and make a query based on tags
            $tag_ids = array();
            foreach($tags as $individual_tag) {
                $tag_ids[] = $individual_tag->term_id;
            }
            $args = array(
                'tag__in' => $tag_ids,
                'post__not_in' => array($id),
                'posts_per_page'=>-1,
                'ignore_sticky_posts'=> 1,
                'post_type' => 'noticia'
            );
            $related_query = new WP_Query( $args );
            // Starting the loop
            if ( $related_query->have_posts()) { ?>
              <section class="noticias">
                <div class="columna">
                  <a id="noticias">
                    <h2 class="epigrafe">Noticias</h2>
                  </a>
                <?php while ($related_query->have_posts()){
                  $related_query->the_post(); ?>
                    <ul>
                      <li>
                        <h3>
                          <a href="<?= get_permalink(); ?>"><?php the_title(); ?>
                          </a>
                        </h3>
                        <?php the_excerpt(); ?>
                      </li>
                    </ul>

                <?php } ?>
                </div>
                </section>
              <?php }
            }
            wp_reset_postdata();
          }?>

          <?php get_related_posts(get_the_ID()); ?>

          <?php
          if (get_field('galerias')) { ?>
            <section class="media">
              <div class="columna">
                <a id="media"><h2 class="epigrafe">Media</h2></a>
                <?php the_field('galerias'); ?>
              </div>
            </section>
          <?php } ?>








    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>

<article <?php post_class(); ?>>
  <div class="imagen">
    <?php the_post_thumbnail('medium'); ?>
  </div>
  <header>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <?php get_template_part('templates/entry-meta'); ?>
  </header>
  <?php $resumen = get_the_excerpt();
  if (!$resumen == '') { ?>
    <div class="entry-summary">
      <?php the_excerpt(); ?>
    </div>
  <?php } ?>
</article>

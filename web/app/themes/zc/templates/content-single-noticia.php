<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>

    <header>
      <div class="columna">
        <a href="<?php echo get_site_url().'/noticias'; ?>" class="enlace">
          <div class="icono">
            <i class="s s-z-der"></i>
            <i class="s s-z-izq"></i>
            <i class="s s-noticias-0"></i>
          </div>
        </a>
        <h1 class="entry-title"><?php the_title(); ?></h1>
        <?php get_template_part('templates/entry-meta'); ?>
      </div>
    </header>


      <?php if ( has_post_thumbnail() ) { ?>
        <div class="imagen">
        <?php the_post_thumbnail('i1200', ['class' => 'columna']); ?>
        </div>
      <?php } ?>


    <?php if (has_excerpt()) { ?>
      <div class="excerpt">
        <div class="columna">
          <?php the_excerpt(); ?>
        </div>
      </div>
    <?php } ?>

    <?php if (get_the_content()) { ?>
      <div class="entry-content">
        <div class="columna">
          <?php the_content(); ?>
        </div>
      </div>
    <?php } ?>


    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>

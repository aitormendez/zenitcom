<?php
$clase = '';
if (has_post_thumbnail()) {
  $clase .= ' imagen';
 }
 if (has_excerpt()) {
   $clase .= ' resumen';
} ?>


<article <?php post_class($clase); ?>>
<div class="columna">
  <div class="los-datos">
    <aside>
      <?php $epigrafe = get_post_type();
      if ($epigrafe == 'page') {
        echo 'página';
      } else {
        echo $epigrafe;
      }?>
    </aside>
    <header>
      <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
      <?php get_template_part('templates/entry-meta'); ?>
    </header>
    <?php if (has_excerpt()) { ?>
      <div class="entry-summary">
        <?php the_excerpt(); ?>
      </div>
    <?php } ?>
  </div>
  <?php if (has_post_thumbnail()) { ?>
    <div class="la-imagen">
      <?php the_post_thumbnail('medium'); ?>
    </div>

  <?php } ?>
</div>
</article>

<?php use Roots\Sage\Titles;

if (is_tax('clientes')) { ?>
  <div class="page-header">
    <div class="cabecera">
      <?php
      $queried_object = get_queried_object();
      $taxonomy = $queried_object->taxonomy;
      $term_id = $queried_object->term_id;
      // load thumbnail for this taxonomy term (term object)
      $image = get_field('logotipo', $queried_object);
      if (!empty($image)) { ?>
        <div class="img-logo">
          <img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
        </div>
      <?php } ?>
      <h1>
        <?= Titles\title(); ?>
      </h1>
    </div>
  </div>
<?php } elseif (is_page('noticias')) { ?>
    <div class="page-header">
      <a href="<?php echo get_site_url().'/noticias'; ?>" class="enlace">
        <div class="icono">
          <i class="s s-z-der"></i>
          <i class="s s-z-izq"></i>
          <i class="s s-noticias-0"></i>
        </div>
        <h1>
          <?= Titles\title(); ?>
        </h1>
      </a>
    </div>
<?php } elseif (is_search()) { ?>
  <div class="page-header">
    <h1 class="columna">
      <?= Titles\title(); ?>
    </h1>
  </div>
<?php } else { ?>
  <div class="page-header">
    <h1>
      <?= Titles\title(); ?>
    </h1>
  </div>
<?php }
?>

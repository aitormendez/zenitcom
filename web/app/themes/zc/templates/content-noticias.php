<article <?php post_class(); ?>>
  <header>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <?php get_template_part('templates/entry-meta'); ?>

    <?php if (has_post_thumbnail()) { ?>
      <div class="imagen">


        <?php
        $post_thumbnail_id = get_post_thumbnail_id( $post->ID );
        $img_src = get_the_post_thumbnail_url($post->ID, 'medium');
        $img_srcset = wp_get_attachment_image_srcset($post_thumbnail_id, 'medium');
        ?>
        <img src="<?php echo esc_url( $img_src ); ?>"
             srcset="<?php echo esc_attr( $img_srcset ); ?>"
             sizes="(max-width: 1000px) calc(50vw - 50px), 311.33px" alt="<?php the_title() ?>">
      </div>
    <?php } ?>

  </header>
  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div>
</article>

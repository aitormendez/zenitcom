<?php
/**
 * Template Name: servicios
 */
?>

<div class="contenido">
<?php
  $args = array(
        'hide_empty' => 0,
  );
  $terms = get_terms( 'servicios', $args );
  if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
      foreach ( $terms as $term ) {
        ?>
        <article>
          <a href="<?php echo get_term_link( $term ); ?>" title="<?php echo $term->name; ?>">
            <h3><?php echo $term->name; ?></h3>
          </a>
          <p>
            <?php echo $term->description; ?>
          </p>
          <?php


          $args = array(
          	'post_type'              => array( 'trabajo' ),
          	'posts_per_page'         => '5',
          	'tax_query'              => array(
          		array(
          			'taxonomy'         => 'servicios',
          			'terms'            => $term->term_id,
          		),
          	),
          );

          $trabajos_query = new WP_Query( $args );
          if ( $trabajos_query->have_posts() ) { ?>
            <ul class="trabajos">
          	<?php while ( $trabajos_query->have_posts() ) {
          		$trabajos_query->the_post(); ?>
              <li>
                <a href="<?php the_permalink(); ?>" alt="<?php the_title(); ?>"><?php the_title(); ?></a>
              </li>
          	 <?php } ?>
             <li class="ver-todos"><a href="<?php echo get_term_link( $term ); ?>" title="<?php echo $term->name; ?>">Ver todos <span>→</span></a></li>
             </ul>
          <?php } ?>
          <?php wp_reset_postdata();
          ?>
        </article>
      <?php }
  }
  ?>
</div>

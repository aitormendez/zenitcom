<?php
/**
 * Template Name: Trabajos
 */
?>


<!-- TRABAJO DESTACADO -->
<?php
$args = array(
  'post_type'              => array( 'trabajo' ),
  'posts_per_page'         => '1',
  'meta_query' => array(
    array(
      'key' => 'importancia', // name of custom field
      'value' => 'mucha',
    )
  )
);
$trabajos_query = new WP_Query( $args );
if ( $trabajos_query->have_posts() ) {
	while ( $trabajos_query->have_posts() ) {
		$trabajos_query->the_post(); ?>
    <article class="grande columna">
      <div class="metadatos">
        <h2>
          <a href="<?= get_permalink($post->ID); ?>"><?php the_title(); ?></a>
        </h2>
        <?php get_template_part('templates/entry-meta');
        the_excerpt();?>

      </div>
      <div class="imagen">
        <?php if ( has_post_thumbnail() ) {
          the_post_thumbnail('i600');
        } ?>
      </div>

    </article>
	<?php }
}
wp_reset_postdata();
 ?>

 <!-- TRABAJOS IMPORTANCIA MEDIA -->
 <?php
 $args = array(
   'post_type'              => array( 'trabajo' ),
   'posts_per_page'         => '3',
   'meta_query' => array(
     array(
       'key' => 'importancia', // name of custom field
       'value' => 'media',
     )
   )
 );
 $trabajos_query = new WP_Query( $args );
 if ( $trabajos_query->have_posts() ) { ?>
   <div class="media columna">
 	<?php while ( $trabajos_query->have_posts() ) {
 		$trabajos_query->the_post(); ?>
     <article class="medio">
       <div class="imagen">
         <?php if ( has_post_thumbnail() ) {
           the_post_thumbnail('i600');
         } ?>
       </div>
       <div class="metadatos">
         <h2>
           <a href="<?= get_permalink($post->ID); ?>"><?php the_title(); ?></a>
         </h2>
         <?php get_template_part('templates/entry-meta'); ?>
       </div>


     </article>
 	<?php } ?>
</div>
 <?php }
 wp_reset_postdata();
  ?>

  <!-- TRABAJOS IMPORTANCIA POCA -->
  <?php
  $args = array(
    'post_type'              => array( 'trabajo' ),
    'posts_per_page'         => -1,
    'meta_query' => array(
      array(
        'key' => 'importancia', // name of custom field
        'value' => 'poca',
      )
    )
  );
  $trabajos_query = new WP_Query( $args );
  if ( $trabajos_query->have_posts() ) { ?>
    <div class="poca columna">
  	<?php while ( $trabajos_query->have_posts() ) {
  		$trabajos_query->the_post(); ?>
      <article class="poco">
        <div class="metadatos">
          <h2>
            <a href="<?= get_permalink($post->ID); ?>"><?php the_title(); ?></a>
          </h2>
          <?php get_template_part('templates/entry-meta'); ?>
        </div>
      </article>
  	<?php } ?>
 </div>
  <?php }
  wp_reset_postdata();
   ?>

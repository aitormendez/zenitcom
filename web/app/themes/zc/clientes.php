<?php
/**
 * Template Name: Clientes
 */
?>

<div class="columna">


<?php
  $args = array(
        'hide_empty' => 0,
  );

  $terms = get_terms( 'clientes', $args );
  if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
      foreach ( $terms as $term ) {
        $image = get_field('logotipo', $term);
        // thumbnail
        $size = 'medium';
        $thumb = $image['sizes'][ $size ];
        $width = $image['sizes'][ $size . '-width' ];
        $height = $image['sizes'][ $size . '-height' ];
        ?>
          <a href="<?php echo get_term_link( $term ); ?>" title="<?php echo $term->name; ?>" role="article">
            <h3><?php echo $term->name; ?></h3>

            <?php if (!empty($image)) { ?>
              <img class="circle" src="<?php echo $thumb; ?>" alt="<?php echo $term->name; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
          <?php } ?>
            <div class="mask"></div>
          </a>
      <?php }
  }
  ?>
</div>
